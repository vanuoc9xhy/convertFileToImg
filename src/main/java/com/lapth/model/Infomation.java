package com.lapth.model;

import org.springframework.web.multipart.MultipartFile;

public class Infomation {
    /**
     * The image file name
     */
    private String fileName;

    /**
     * The number of image file generated from given PDF file
     */
    private Integer count;

    private String formatFile;

    private String status;

    private MultipartFile file;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getFormatFile() {
        return formatFile;
    }

    public void setFormatFile(String formatFile) {
        this.formatFile = formatFile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Infomation(MultipartFile file) {
        this.file = file;
    }

    public Infomation() {

    }
    }
