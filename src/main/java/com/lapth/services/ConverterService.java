package com.lapth.services;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.DocumentFamily;
import com.artofsolving.jodconverter.DocumentFormat;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.StreamOpenOfficeDocumentConverter;
import com.lapth.model.Infomation;
import com.lapth.utils.FileManager;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;

@Service
public class ConverterService implements IConverterService {
    @Value("${converted.images.folder.home}")
    private String folderHome;

    @Value("${converted.images.folder.pdf}")
    private String folderPDF;

    @Value("${converted.images.folder.img}")
    private String folderIMG;

    @Value("${converted.images.text}")
    private String stringWaterMark;

    @Value("${converted.port}")
    private int portOpenOffice;

    @Value("${converted.host}")
    private String hostOpenOffice;

    public Infomation convertPdfToPng(String fileName) throws IOException {
        long start = System.currentTimeMillis();
        Infomation infomation = new Infomation();
        InputStream inputStream = new FileInputStream(fileName);

        PDDocument pdfFile = PDDocument.load(inputStream);
        PDFRenderer pdfRenderer = new PDFRenderer(pdfFile);

        String[] maxLenght = fileName.split("[\\/|\\\\]");
        String fileImgName = maxLenght[maxLenght.length - 1].split("\\.")[0];

        // Duyệt qua toàn bộ các trang PDF
        for (int pageNumber = 0; pageNumber < pdfFile.getNumberOfPages(); ++pageNumber) {
            BufferedImage bim = pdfRenderer.renderImage(pageNumber);

            String destDir = folderIMG + fileImgName + "_" + pageNumber + ".png";

            ImageIO.write(bim, "png", new File(destDir));
            String text = stringWaterMark;

            // adding text as overlay to an image
            addTextWatermark(text, "png", bim, new File(destDir));
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("PDF to IMG + WaterMark: " + time);
        pdfFile.close();
        infomation.setFormatFile("png");
        infomation.setCount(pdfFile.getNumberOfPages());
        infomation.setFileName(folderIMG);
        return infomation;
    }


    private void addTextWatermark(String text, String type, BufferedImage image, File destination) throws IOException, IOException {

        int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
        BufferedImage watermarked = new BufferedImage(image.getWidth(), image.getHeight(), imageType);
        Graphics2D w = (Graphics2D) watermarked.getGraphics();
        w.drawImage(image, 0, 0, null);
        AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f);
        w.setComposite(alphaChannel);
        w.setColor(Color.RED);
        w.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 26));
        FontMetrics fontMetrics = w.getFontMetrics();
        Rectangle2D rect = fontMetrics.getStringBounds(text, w);

        // calculate center of the image
        int centerX = (image.getWidth() - (int) rect.getWidth()) / 2;
        int centerY = image.getHeight() / 2;

        // add text overlay to the image
        w.drawString(text, centerX, centerY);
        ImageIO.write(watermarked, type, destination);
        w.dispose();
    }

    public Infomation convertOfficeToPDF(MultipartFile officeFile) throws IOException {
        String path = saveFile(officeFile);
        Infomation infomation = new Infomation();
        long start = System.currentTimeMillis();
        File inputFile = new File(path);
        String[] nameInput = inputFile.getName().split("\\.");
        System.out.println(nameInput);
        String pdfName = folderPDF + nameInput[0] + ".pdf";
        System.out.println(pdfName);
        File outputFile = new File(pdfName);
        // connect to an OpenOffice.org instance running on port 8100
        OpenOfficeConnection connection = new SocketOpenOfficeConnection(hostOpenOffice, portOpenOffice);
        connection.connect();

        // convert
        DocumentConverter converter = new StreamOpenOfficeDocumentConverter(connection);
        final DocumentFormat docx = new DocumentFormat("Microsoft Word 2007 XML", DocumentFamily.TEXT, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
        converter.convert(inputFile, docx, outputFile, null);
        connection.disconnect();
        long time = System.currentTimeMillis() - start;
        System.out.println("file to PDF: " + time);
        infomation.setCount(1);
        infomation.setFileName(pdfName);
        infomation.setFormatFile("pdf");
        return infomation;
    }

    private String saveFile(MultipartFile file) throws IOException {
        FileManager fileManager = new FileManager();

        String nameFile = new Date().getTime() + "." + fileManager.getFormatFile(file.getOriginalFilename());

        String path = folderHome + nameFile;

        fileManager.createNewMultiPartFile(path, file);
        return path;
    }

    public void convertAll(MultipartFile officeFile) throws IOException {

//        Runnable runnable = () -> {
//            try {
//                RestTemplate restTemplate = new RestTemplate();
//                HttpEntity<Infomation> request = new HttpEntity<>(new Infomation(officeFile));
//                Infomation info = restTemplate.postForObject("http://localhost:8888/convertFileToPdf", request, Infomation.class);
//
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//        };
//        Thread thread = new Thread(runnable);
//        thread.start();
//        Runnable runnable2 = () -> {
//            try {
//
//                RestTemplate restTemplate = new RestTemplate();
//                HttpEntity<Infomation> request = new HttpEntity<>(new Infomation(officeFile));
//                Infomation info = restTemplate.postForObject("http://localhost:8888/convertFileToPdf", request, Infomation.class);
//
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//        };
//        Thread thread = new Thread(runnable);
//        thread.start();

        Infomation infomation = convertOfficeToPDF(officeFile);
        convertPdfToPng(infomation.getFileName());
    }

    @Override
    public File dowwnloadImage(String nameImage) throws IOException {

        String path = folderIMG  + nameImage;

        return new File(path);
    }

}
