package com.lapth.services;

import com.lapth.model.Infomation;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface IConverterService {

    public Infomation convertPdfToPng(String fileName) throws IOException;

    public Infomation convertOfficeToPDF(MultipartFile officeFile) throws IOException;

    public void convertAll(MultipartFile officeFile) throws IOException;

    File dowwnloadImage(String nameImage) throws IOException;
}
