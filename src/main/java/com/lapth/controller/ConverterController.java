package com.lapth.controller;

import com.lapth.model.Infomation;
import com.lapth.services.IConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
public class ConverterController {

    @Autowired
    private IConverterService pdf2ImageService;

    @GetMapping(path = "/ping")
    public String ping() {
        return "Hello, you are here!";
    }

    @PostMapping("/convert")
    public void convertFileToPDF_Image(MultipartFile officeFile) throws Exception {
        long start = System.currentTimeMillis();
        try {
            pdf2ImageService.convertAll(officeFile);
            long time = System.currentTimeMillis() - start;
            System.out.println(time);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    @PostMapping("/convertFileToPdf")
    public ResponseEntity<Infomation>  convertFileToPdf(MultipartFile officeFile) throws Exception {
        Infomation infomation = new Infomation();
        long start = System.currentTimeMillis();
        try {
            // convert To PDF and save file
            infomation = pdf2ImageService.convertOfficeToPDF(officeFile);
            long time = System.currentTimeMillis() - start;
            System.out.println(time);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return new ResponseEntity<Infomation>(infomation, HttpStatus.OK);
    }

    @PostMapping("/convertPdfToPng")
    public Infomation convertPdfToPng(String fileName) throws Exception {
        Infomation infomation = new Infomation();
        long start = System.currentTimeMillis();
        try {
            // convert PDF to Png
            infomation = pdf2ImageService.convertPdfToPng(fileName);
            long time = System.currentTimeMillis() - start;
            System.out.println(time);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return infomation;
    }

    @GetMapping(value = "img/{nameImage}")
    public ResponseEntity<?> downloadImage(@PathVariable String nameImage) throws IOException {

        // TODO validate
//        Resource resource = new ClassPathResource("classpath:/application/context/references/user/user.xml");
//        File file = resource.getFile();

        File imageFile = pdf2ImageService.dowwnloadImage(nameImage);
        InputStreamResource imageStream = new InputStreamResource(new FileInputStream(imageFile));

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(MediaType.IMAGE_JPEG_VALUE))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${System.currentTimeMillis()}\"")
                .body(imageStream);

    }

}
