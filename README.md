## Các bước thực hiện
### Build
```shell script
mvn clean package
```
### Copy `pdf2image.jar` vào thư mục `docker`
```shell script
cp target/pdf2image.jar docker/
```
### Build docker image
```shell script
cd docker
docker build . 
```
### Test docker image 
Xem readme trong thư mục release

## Chạy service ở môi trường DEV
### Khởi chạy Open Office từ docker
```shell script
  - Di chuyển cmd tới vị trí dockerfile của open Office và chạy:

docker build -t openoffice .
docker run -dp 8100:8100 --name openoffice openoffice
  
  - Hoặc chạy trên dockerhub:
  
docker run -d --name=openoffice -p 8100:8100 bdhhbdhh/openoffice4
```
### khởi chạy project
```
mvn spring-boot:run -Dspring.config.location=path\to\config\application.properties

Cấu hình ở file application.properties phù hợp:
    
    - converted.images.folder.pdf : đường dẫn thư mục để lưu file pdf
    - converted.images.folder.img : đường dẫn thư mục để lưu file image
    - converted.images.folder.home : đường dẫn thư mục để lưu file upload
    - converted.images.text : nội dung trên waterMark
```

# Các API
- GET: /ping (test trạng thái của project đã hoạt động chưa)
- POST: /convert (lưu các file office vào thư mục được cấu hình, convert các file đó sang pdf và sang image có water mark )
  

  ![alt text]( https://i.imgur.com/MtBFaxz.png)

- POST: /convertFileToPdf (lưu các file office vào thư mục được cấu hình, convert các file đó sang pdf)


![alt text]( https://i.imgur.com/iDLYpwM.png)

- POST: /convertPdfToPng (lấy các file có sẵn trong folder PDF, convert các file đó sang image có water mark )


![alt text]( https://i.imgur.com/8j6ucr4.png)
